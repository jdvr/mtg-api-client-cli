# Magic: The Gathering API Client CLI
---

Codetest asked by a company to develop a cli using just standard library of any language.

## Requirement

- [node](https://nodejs.org) v13 or above

## Test

Run test using `npm test`

## Usage:

This client make use of **[Magic: The Gathering (MTG) API](https://docs.magicthegathering.io/)** to retrieve three things:

* Returns a list of **Cards** grouped by **`set`**.
```shell
./run.sh [--grouped-by-set] # optional flag, default behavior
```
* Returns a list of **Cards** grouped by **`set`** and then each **`set`** grouped by **`rarity`**.
```shell
./run.sh --grouped-by-rarity
```
* Returns a list of cards from the  **Khans of Tarkir (KTK)** that ONLY have the colours `red` AND `blue`
```shell
./run.sh --ktk-red-blue
```

## Notes:

- I have added a `./run` script to wrap command runner on docker and avoid node version problems. You can run with your system node if your version is compatible
- Zero external dependecies.
- If rate limit is reached you will get and error message
- There is a parallelism problem, I known it could be _"easily"_ fixed by making group of request to avoid sever to goes down but I guess current implementation is enough
- Project folder and structures is designed thinking in keep it simple but at least enough isolate to make test as easier as I could
- Please review git history (reverse) to get context on how code was created