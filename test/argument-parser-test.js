const assert = require("assert").strict;
const argumentParser = require("../src/argument-parser");
const { withArgument, DEFAULT_ARGV } = require("./mother/arguments-mother");
const {
  GROUPED_BY_SET,
  GROUPED_BY_RARITY,
  KTK_RED_BLUE,
  PRINT_HELP
} = require("../src/constants/behaviour");
const {
  GROUPED_BY_SET: ARG_GROUPED_BY_SET,
  GROUPED_BY_RARITY: ARG_GROUPED_BY_RARITY,
  KTK_RED_BLUE: ARG_KTK_RED_BLUE,
  PRINT_HELP: ARG_PRINT_HELP
} = require("../src/constants/argument");

module.exports = {
  suite: "Argument Parser",
  tests: [
    {
      name: "Missing arguments returns GROUPED_BY_SET as behaviour",
      run() {
        assert.equal(argumentParser(DEFAULT_ARGV), GROUPED_BY_SET);
      }
    },
    {
      name: "Invalid argument returns PRINT_HELP as behaviour",
      run() {
        assert.equal(
          argumentParser(withArgument("any-invalid-argument")),
          PRINT_HELP
        );
      }
    },
    {
      name: "-h argument returns PRINT_HELP as behaviour",
      run() {
        assert.equal(argumentParser(withArgument(ARG_PRINT_HELP)), PRINT_HELP);
      }
    },
    {
      name: "--grouped-by-set argument returns GROUPED_BY_SET as behaviour",
      run() {
        assert.equal(
          argumentParser(withArgument(ARG_GROUPED_BY_SET)),
          GROUPED_BY_SET
        );
      }
    },
    {
      name: "--grouped-by-rarity argument returns GROUPED_BY_SET as behaviour",
      run() {
        assert.equal(
          argumentParser(withArgument(ARG_GROUPED_BY_RARITY)),
          GROUPED_BY_RARITY
        );
      }
    },
    {
      name: "--ktk-red-blue argument returns KTK_RED_BLUE as behaviour",
      run() {
        assert.equal(
          argumentParser(withArgument(ARG_KTK_RED_BLUE)),
          KTK_RED_BLUE
        );
      }
    },
    {
      name: "more than one argument returns PRINT_HELP as behaviour",
      run() {
        const argv = DEFAULT_ARGV.concat([
          "--ktk-red-blue",
          "--grouped-by-set"
        ]);
        assert.equal(argumentParser(argv), PRINT_HELP);
      }
    }
  ]
};
