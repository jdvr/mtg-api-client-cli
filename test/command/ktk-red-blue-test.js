const assert = require("assert").strict;
const ktkRedBlue = require("../../src/command/ktk-red-blue");
const { ALL_CARDS, KTK_RED_BLUE_CARDS } = require("../mother/card-mother");

module.exports = {
  suite: "KTK set red and blue colors filter command",
  tests: [
    {
      name:
        "will filter from list of cards a list of KTK set red and blue cards",
      run() {
        const ktkRedBlueCards = ktkRedBlue(ALL_CARDS);
        assert.deepEqual(ktkRedBlueCards, KTK_RED_BLUE_CARDS);
      }
    }
  ]
};
