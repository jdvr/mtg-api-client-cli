const assert = require("assert").strict;
const cliArguments = require("../../src/constants/argument");
const { values } = require("../../src/constants/utils");
const printHelp = require("../../src/command/print-help");

module.exports = {
  suite: "Print Help Command",
  tests: [
    {
      name: "Help message includes arguments definition",
      run() {
        const help = printHelp();
        values(cliArguments).forEach(arg => assert.ok(help.includes(arg)));
      }
    }
  ]
};
