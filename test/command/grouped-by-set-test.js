const assert = require("assert").strict;
const groupedBySet = require("../../src/command/grouped-by-set");
const { ALL_CARDS, BY_SET } = require("../mother/card-mother");

module.exports = {
  suite: "Grouped by set command",
  tests: [
    {
      name:
        "will transform a list of cards into a map setCode -> list of cards",
      run() {
        const groupedBySetCards = groupedBySet(ALL_CARDS);
        assert.deepEqual(groupedBySetCards, BY_SET);
      }
    }
  ]
};
