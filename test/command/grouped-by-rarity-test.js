const assert = require("assert").strict;
const groupedByRarity = require("../../src/command/grouped-by-rarity");
const { ALL_CARDS, BY_RARITY } = require("../mother/card-mother");

module.exports = {
  suite: "Grouped by Rarity command",
  tests: [
    {
      name:
        "will transform a list of cards into a map setCode -> rarity -> list of cards",
      run() {
        const groupedByRarityCards = groupedByRarity(ALL_CARDS);
        assert.deepEqual(groupedByRarityCards, BY_RARITY);
      }
    }
  ]
};
