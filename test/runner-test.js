const assert = require("assert").strict;
const createRunner = require("../src/runner");
const { withArgument } = require("./mother/arguments-mother");
const argumentParser = require("../src/argument-parser");
const commandFactory = require("../src/command");
const printHelpCommand = require("../src/command/print-help");
const {
  ALL_CARDS,
  BY_SET,
  BY_RARITY,
  KTK_RED_BLUE_CARDS
} = require("./mother/card-mother");
const {
  PRINT_HELP,
  GROUPED_BY_SET,
  GROUPED_BY_RARITY,
  KTK_RED_BLUE
} = require("../src/constants/argument");

const MOCKED_API_CLIENT = {
  getCards: () => Promise.resolve(ALL_CARDS)
};
const EXPECTED_API_ERROR = new Error("Expected api error");
const MOCKED_FAIL_API_CLIENT = {
  getCards: () => Promise.reject(EXPECTED_API_ERROR)
};

module.exports = {
  suite: "Runner",
  tests: [
    {
      name: "Given help argument command runner will run help command",
      run() {
        const runner = createRunner(
          argumentParser,
          MOCKED_API_CLIENT,
          commandFactory
        );
        runner
          .run(withArgument(PRINT_HELP))
          .then(r => assert.equal(r, printHelpCommand()));
      }
    },
    {
      name: "Given an api error, runner will fail",
      run() {
        const DEFAULT_ARGV = ["/usr/local/bin/node", "/usr/src/app/index.js"];
        const failedApiMockRunner = createRunner(
          argumentParser,
          MOCKED_FAIL_API_CLIENT,
          commandFactory
        );
        failedApiMockRunner
          .run(DEFAULT_ARGV)
          .then(() =>
            assert.fail("expecting and error, but everything goes well")
          )
          .catch(err => assert.equal(err.message, EXPECTED_API_ERROR.message));
      }
    },
    {
      name: "Given a command error, runner will propagate the error",
      run() {
        const DEFAULT_ARGV = ["/usr/local/bin/node", "/usr/src/app/index.js"];
        let expectedCommandErrorMessage = "command error";
        const failedCommandFactory = () => ({
          requireApi: false,
          run: () => {
            throw Error(expectedCommandErrorMessage);
          }
        });
        const failedApiMockRunner = createRunner(
          argumentParser,
          MOCKED_API_CLIENT,
          failedCommandFactory
        );
        failedApiMockRunner
          .run(DEFAULT_ARGV)
          .then(() =>
            assert.fail("expecting and error, but everything goes well")
          )
          .catch(err => assert.equal(err.message, expectedCommandErrorMessage));
      }
    },
    {
      name: "Given grouped by set argument will return cards grouped by set",
      run() {
        const runner = createRunner(
          argumentParser,
          MOCKED_API_CLIENT,
          commandFactory
        );
        runner
          .run(withArgument(GROUPED_BY_SET))
          .then(result => assert.deepEqual(result, BY_SET));
      }
    },
    {
      name:
        "Given grouped by rarity argument will return cards grouped by set and rarity",
      run() {
        const runner = createRunner(
          argumentParser,
          MOCKED_API_CLIENT,
          commandFactory
        );
        runner
          .run(withArgument(GROUPED_BY_RARITY))
          .then(result => assert.deepEqual(result, BY_RARITY));
      }
    },
    {
      name:
        "Given ktk red blue argument will return cards filtered by ktk set and red and blue colors",
      run() {
        const runner = createRunner(
          argumentParser,
          MOCKED_API_CLIENT,
          commandFactory
        );
        runner
          .run(withArgument(KTK_RED_BLUE))
          .then(result => assert.deepEqual(result, KTK_RED_BLUE_CARDS));
      }
    }
  ]
};
