const assert = require("assert").strict;
const { range } = require("../src/utils");

module.exports = {
  suite: "Util module",
  tests: [
    {
      name: "Range",
      run() {
        assert.deepEqual(range(0, 2), [0, 1, 2]);
        assert.deepEqual(range(2, 5), [2, 3, 4, 5]);
        assert.deepEqual(range(8, 3), []);
      }
    }
  ]
};
