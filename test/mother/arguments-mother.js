const DEFAULT_ARGV = ["/usr/local/bin/node", "/usr/src/app/index.js"];

module.exports = {
  DEFAULT_ARGV,
  withArgument: argument => DEFAULT_ARGV.concat(`--${argument}`)
};
