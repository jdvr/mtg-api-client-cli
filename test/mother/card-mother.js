const ALL_CARDS = [
  {
    name: "Any Card Name 1",
    colors: ["White"],
    rarity: "Uncommon",
    set: "A",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 2",
    colors: ["Red"],
    rarity: "Mythic Rare",
    set: "B",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 3",
    colors: ["White"],
    rarity: "Common",
    set: "A",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 4",
    colors: ["White"],
    rarity: "Mythic Rare",
    set: "B",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 5",
    colors: ["Blue"],
    rarity: "Common",
    set: "A",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 6",
    colors: ["White"],
    rarity: "Special",
    set: "B",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 7",
    colors: ["Red", "White"],
    rarity: "Rare",
    set: "KTK",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 8",
    colors: ["Red", "Blue"],
    rarity: "Rare",
    set: "KTK",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  },
  {
    name: "Any Card Name 9",
    colors: ["White", "Blue"],
    rarity: "Rare",
    set: "KTK",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  }
];

const BY_RARITY = {
  A: {
    Uncommon: [
      {
        name: "Any Card Name 1",
        colors: ["White"],
        rarity: "Uncommon",
        set: "A",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      }
    ],
    Common: [
      {
        name: "Any Card Name 3",
        colors: ["White"],
        rarity: "Common",
        set: "A",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      },
      {
        name: "Any Card Name 5",
        colors: ["Blue"],
        rarity: "Common",
        set: "A",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      }
    ]
  },
  B: {
    "Mythic Rare": [
      {
        name: "Any Card Name 2",
        colors: ["Red"],
        rarity: "Mythic Rare",
        set: "B",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      },
      {
        name: "Any Card Name 4",
        colors: ["White"],
        rarity: "Mythic Rare",
        set: "B",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      }
    ],
    Special: [
      {
        name: "Any Card Name 6",
        colors: ["White"],
        rarity: "Special",
        set: "B",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      }
    ]
  },
  KTK: {
    Rare: [
      {
        name: "Any Card Name 7",
        colors: ["Red", "White"],
        rarity: "Rare",
        set: "KTK",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      },
      {
        name: "Any Card Name 8",
        colors: ["Red", "Blue"],
        rarity: "Rare",
        set: "KTK",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      },
      {
        name: "Any Card Name 9",
        colors: ["White", "Blue"],
        rarity: "Rare",
        set: "KTK",
        id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
      }
    ]
  }
};

const BY_SET = {
  A: [
    {
      name: "Any Card Name 1",
      colors: ["White"],
      rarity: "Uncommon",
      set: "A",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    },
    {
      name: "Any Card Name 3",
      colors: ["White"],
      rarity: "Common",
      set: "A",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    },
    {
      name: "Any Card Name 5",
      colors: ["Blue"],
      rarity: "Common",
      set: "A",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    }
  ],
  B: [
    {
      name: "Any Card Name 2",
      colors: ["Red"],
      rarity: "Mythic Rare",
      set: "B",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    },
    {
      name: "Any Card Name 4",
      colors: ["White"],
      rarity: "Mythic Rare",
      set: "B",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    },
    {
      name: "Any Card Name 6",
      colors: ["White"],
      rarity: "Special",
      set: "B",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    }
  ],
  KTK: [
    {
      name: "Any Card Name 7",
      colors: ["Red", "White"],
      rarity: "Rare",
      set: "KTK",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    },
    {
      name: "Any Card Name 8",
      colors: ["Red", "Blue"],
      rarity: "Rare",
      set: "KTK",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    },
    {
      name: "Any Card Name 9",
      colors: ["White", "Blue"],
      rarity: "Rare",
      set: "KTK",
      id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
    }
  ]
};

const KTK_RED_BLUE_CARDS = [
  {
    name: "Any Card Name 8",
    colors: ["Red", "Blue"],
    rarity: "Rare",
    set: "KTK",
    id: "02ea5ddc89d7847abc77a0fbcbf2bc74e6456559"
  }
];

module.exports = {
  ALL_CARDS,
  BY_SET,
  BY_RARITY,
  KTK_RED_BLUE_CARDS
};
