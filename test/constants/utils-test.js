const assert = require("assert").strict;
const { values } = require("../../src/constants/utils");

module.exports = {
  suite: "Constants Util",
  tests: [
    {
      name: "values",
      run() {
        const expectedValues = [1, 2, 3];

        assert.deepEqual(
          values({
            one: 1,
            two: 2,
            three: 3
          }),
          expectedValues
        );
      }
    }
  ]
};
