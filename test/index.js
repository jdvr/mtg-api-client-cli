const argumentParserSuite = require("./argument-parser-test");
const printHelpCommandSuite = require("./command/print-help-test");
const constantsUtilsSuite = require("./constants/utils-test");
const runnerSuite = require("./runner-test");
const utilsTest = require("./utils-test");
const groupedBySetSuite = require("./command/grouped-by-set-test");
const groupedByRaritySuite = require("./command/grouped-by-rarity-test");
const ktkRedBlueSuite = require("./command/ktk-red-blue-test");

const passedSuites = [];

[
  argumentParserSuite,
  printHelpCommandSuite,
  constantsUtilsSuite,
  runnerSuite,
  utilsTest,
  groupedBySetSuite,
  groupedByRaritySuite,
  ktkRedBlueSuite
].forEach(({ suite, tests }) => {
  console.log("\x1b[36m%s\x1b[0m", `Running Suite: "${suite}"`);
  tests.forEach(({ name, run }) => {
    console.log("\x1b[33m%s\x1b[0m", `\t - ${name}`);
    run();
  });
  passedSuites.push(suite);
});

passedSuites.forEach(s => console.log("\x1b[92m%s\x1b[0m", `✔ "${s}"`));
