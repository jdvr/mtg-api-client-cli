const util = require("util");
const createApiClient = require("./src/api/client");
const commandFactory = require("./src/command");
const argumentParser = require("./src/argument-parser");
const createRunner = require("./src/runner");

const OUTPUT_FORMAT_PREFERENCES = {
  showHidden: false,
  depth: null,
  colors: true,
  maxArrayLength: null
};

const FIELDS_TO_KEEP_FROM_THE_RESPONSE = [
  "id",
  "set",
  "name",
  "colors",
  "rarity"
];

const runner = createRunner(
  argumentParser,
  createApiClient(FIELDS_TO_KEEP_FROM_THE_RESPONSE),
  commandFactory
);

runner
  .run(process.argv)
  .then(output => {
    if (typeof output === "string") {
      console.log(output);
    } else {
      console.log(util.inspect(output, OUTPUT_FORMAT_PREFERENCES));
    }
  })
  .catch(err => console.error(err));
