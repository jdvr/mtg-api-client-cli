module.exports = {
  values: constants => Object.entries(constants).map(([_, v]) => v)
};
