const arguments = require("../constants/argument");
const { values } = require("../constants/utils");

const USAGE_MESSAGE =
  "Usage: node index [argument]" +
  "\nWhere optional argument could be one of the following:" +
  values(arguments).reduce((acc, val) => `${acc}\n\t--${val}`, "");

module.exports = () => USAGE_MESSAGE;
