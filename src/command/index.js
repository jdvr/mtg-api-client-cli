const {
  GROUPED_BY_SET,
  GROUPED_BY_RARITY,
  KTK_RED_BLUE,
  PRINT_HELP
} = require("../constants/behaviour");
const printHelp = require("./print-help");
const groupedBySet = require("./grouped-by-set");
const groupedByRarity = require("./grouped-by-rarity");
const ktkRedBlue = require("./ktk-red-blue");

const requireApiCommand = command => ({
  requireApi: true,
  run: command
});

const localCommand = command => ({
  requireApi: false,
  run: command
});

const COMMAND_BY_BEHAVIOUR = {
  [GROUPED_BY_SET]: requireApiCommand(groupedBySet),
  [GROUPED_BY_RARITY]: requireApiCommand(groupedByRarity),
  [KTK_RED_BLUE]: requireApiCommand(ktkRedBlue),
  [PRINT_HELP]: localCommand(printHelp)
};

module.exports = behaviour => COMMAND_BY_BEHAVIOUR[behaviour];
