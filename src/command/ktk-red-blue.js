module.exports = cards =>
  cards.filter(
    card =>
      card.set === "KTK" &&
      card.colors.includes("Blue") &&
      card.colors.includes("Red")
  );
