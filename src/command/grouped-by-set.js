const groupBy = require("./group-by");

module.exports = cards => groupBy(cards, "set");
