const groupBy = require("./group-by");

module.exports = cards =>
  Object.entries(groupBy(cards, "set"))
    .map(([set, cards]) => [set, groupBy(cards, "rarity")])
    .reduce((bySetAndRarity, [set, byRarityCards]) => {
      bySetAndRarity[set] = byRarityCards;
      return bySetAndRarity;
    }, {});
