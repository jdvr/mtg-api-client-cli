module.exports = (cards, property) =>
  cards.reduce((byProperty, card) => {
    const propertyCards = byProperty[card[property]] || [];
    propertyCards.push(card);
    byProperty[card[property]] = propertyCards;
    return byProperty;
  }, {});
