const https = require("https");
const { range } = require("../utils");

const FIRST_PAGE = 1;
const MAX_PARALLEL_REQUEST = 150;

const requestPage = (page, onSuccess, onError) => {
  console.info(`[CLIENT] Requesting page ${page}...`);
  https
    .get(`https://api.magicthegathering.io/v1/cards?page=${page}`, res => {
      const { statusCode } = res;
      if (statusCode !== 200) {
        // Consume response data to free up memory
        res.resume();
        onError(new Error(`Request Failed\nStatus Code: ${statusCode}`));
      }
      const { headers } = res;
      const pageSize = headers["page-size"];
      const totalCount = headers["total-count"];
      const rateLimitRemaining = headers["ratelimit-remaining"];

      res.setEncoding("utf8");
      let rawData = "";
      res.on("data", chunk => {
        rawData += chunk;
      });
      res.on("end", () => {
        try {
          onSuccess({
            numberOfPages: Math.min(
              Math.ceil(totalCount / pageSize),
              rateLimitRemaining
            ),
            currentPageCards: JSON.parse(rawData).cards
          });
        } catch (e) {
          onError(e);
        }
      });
    })
    .on("error", e => {
      onError(`Got error: ${e.message}`);
    });
};

const getCards = () =>
  new Promise((resolve, reject) => {
    requestPage(FIRST_PAGE, resolve, reject);
  }).then(response => {
    // If I try to run 500 request at the same time I get a 503
    const limit = Math.min(response.numberOfPages, MAX_PARALLEL_REQUEST);
    const requestAllPages = range(2, limit).map(
      page =>
        new Promise((resolve, reject) => requestPage(page, resolve, reject))
    );
    return Promise.all(requestAllPages)
      .then(responses =>
        Promise.resolve(
          responses
            .map(r => r.currentPageCards)
            .concat(response.currentPageCards)
            .flat()
        )
      )
      .catch(err => Promise.reject(err));
  });

module.exports = (fieldsToKeep = []) => ({
  getCards: () => {
    const cardsPromise = getCards();
    if (fieldsToKeep.length === 0) {
      return cardsPromise;
    }
    return cardsPromise.then(cards =>
      cards.map(card => {
        const newCard = {};
        fieldsToKeep.forEach(field => (newCard[field] = card[field]));
        return newCard;
      })
    );
  }
});
