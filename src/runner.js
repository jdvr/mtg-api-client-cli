const createRunner = (argumentParser, apiClient, commandFactory) => ({
  run: processArgv => {
    const behavior = argumentParser(processArgv);
    const command = commandFactory(behavior);
    if (!command.requireApi) {
      return new Promise((resolve, reject) => {
        try {
          resolve(command.run());
        } catch (e) {
          reject(e);
        }
      });
    }
    console.info(
      "\x1b[37m%s\x1b[0m",
      "[INFO] Retrieving information from API..."
    );
    return apiClient
      .getCards()
      .then(cards => command.run(cards))
      .catch(err => {
        console.error(
          "\x1b[31m%s\x1b[0m",
          `Sothing went wrong while processing ${behavior}`
        );
        return Promise.reject(err);
      });
  }
});

module.exports = createRunner;
