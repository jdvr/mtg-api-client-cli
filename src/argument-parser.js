const {
  GROUPED_BY_SET,
  GROUPED_BY_RARITY,
  KTK_RED_BLUE,
  PRINT_HELP
} = require("./constants/behaviour");
const arguments = require("./constants/argument");

const BEHAVIOUR_BY_ARGUMENT = {
  [arguments.GROUPED_BY_SET]: GROUPED_BY_SET,
  [arguments.GROUPED_BY_RARITY]: GROUPED_BY_RARITY,
  [arguments.KTK_RED_BLUE]: KTK_RED_BLUE,
  [arguments.PRINT_HELP]: PRINT_HELP
};

module.exports = argv => {
  if (argv.length > 3) {
    return PRINT_HELP;
  }

  if (argv.length < 3) {
    return GROUPED_BY_SET;
  }

  const argument = argv[2].replace("--", "");
  return BEHAVIOUR_BY_ARGUMENT[argument] || PRINT_HELP;
};
