module.exports = {
  range(start, end) {
    const length = end - (start - 1);
    if (length <= 0) {
      return [];
    }
    return [...Array(length).keys()].map(i => i + start);
  }
};
